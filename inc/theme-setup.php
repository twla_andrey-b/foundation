<?php

if ( ! function_exists('foundation_setup') ) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function foundation_setup() {
        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        // This feature adds RSS feed links to HTML <head>
        add_theme_support( 'automatic-feed-links' );

        //  Add widget support shortcodes
        add_filter('widget_text', 'do_shortcode');

        // Support for Featured Images
        add_theme_support( 'post-thumbnails' );

        // Custom Background
        add_theme_support( 'custom-background', array('default-color' => 'fff'));

        // Custom Header
        add_theme_support( 'custom-header', array(
            'default-image' => get_template_directory_uri() . '/images/custom-logo.png',
            'height'        => '200',
            'flex-height'   => true,
            'uploads'       => true,
            'header-text'   => false
        ) );

        // Register Navigation Menu
        register_nav_menus( array(
            'header-menu' => 'Header Menu',
            'footer-menu' => 'Footer Menu'
        ) );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );
    }
    add_action( 'after_setup_theme', 'foundation_setup' );

endif; // foundation_setup


if ( ! function_exists( 'display_custom_image_sizes' ) ) :
    /**
     * This function gives opportunity to ddd custom image sizes to WP Editor
     */
    function display_custom_image_sizes($sizes) {
        global $_wp_additional_image_sizes;

        if (empty($_wp_additional_image_sizes))
            return $sizes;

        foreach ($_wp_additional_image_sizes as $id => $data) {
            if (!isset($sizes[$id]))
                $sizes[$id] = ucfirst(str_replace('-', ' ', $id));
        }

        return $sizes;
    }
    add_filter('image_size_names_choose', 'display_custom_image_sizes');

endif; // display_custom_image_sizes

// Header Links
if (!function_exists('theme_header_links')) :
    function theme_header_links() {
        wp_nav_menu(array(
            'container'      => false, // Remove nav container
            'menu_class'     => 'vertical medium-horizontal menu', // Adding custom nav class
            'items_wrap'     => '<ul id="%1$s" class="%2$s" data-parent-link="true" data-responsive-menu="drilldown medium-dropdown">%3$s</ul>',
            'theme_location' => 'header-menu', // Where it's located in the theme
            'depth'          => 5, // Limit the depth of the nav
            'fallback_cb'    => false, // Fallback function (see below)
            'walker'         => new Foundation_Menu_Walker()
        ));
    }
endif;

// Big thanks to Brett Mason (https://github.com/brettsmason) for the awesome walker
class Foundation_Menu_Walker extends Walker_Nav_Menu {
    function start_lvl(&$output, $depth = 0, $args = Array()) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"menu\">\n";
    }
}

// Header Fallback Menu
if (!function_exists('header_links_fallback')) :
    function header_links_fallback() {
        wp_page_menu(array(
            'show_home'   => true,
            'menu_class'  => '', // Adding custom nav class
            'include'     => '',
            'exclude'     => '',
            'echo'        => true,
            'link_before' => '', // Before each link
            'link_after'  => '' // After each link
        ));
    }
endif;

// Add Foundation active class to menu
if (!function_exists('required_active_nav_class')) :
    function required_active_nav_class($classes, $item) {
        if ($item->current == 1 || $item->current_item_ancestor == true) {
            $classes[] = 'active';
        }
        return $classes;
    }

    add_filter('nav_menu_css_class', 'required_active_nav_class', 10, 2);
endif;

if ( ! function_exists( 'foundation_pagination' ) ) :
        function foundation_pagination($before = '', $after = '') {
            global $wpdb, $wp_query;
            $request = $wp_query->request;
            $posts_per_page = intval(get_query_var('posts_per_page'));
            $paged = intval(get_query_var('paged'));
            $numposts = $wp_query->found_posts;
            $max_page = $wp_query->max_num_pages;
            if ($numposts <= $posts_per_page) {
                return;
            }
            if (empty($paged) || $paged == 0) {
                $paged = 1;
            }
            $pages_to_show = 7;
            $pages_to_show_minus_1 = $pages_to_show - 1;
            $half_page_start = floor($pages_to_show_minus_1 / 2);
            $half_page_end = ceil($pages_to_show_minus_1 / 2);
            $start_page = $paged - $half_page_start;
            if ($start_page <= 0) {
                $start_page = 1;
            }
            $end_page = $paged + $half_page_end;
            if (($end_page - $start_page) != $pages_to_show_minus_1) {
                $end_page = $start_page + $pages_to_show_minus_1;
            }
            if ($end_page > $max_page) {
                $start_page = $max_page - $pages_to_show_minus_1;
                $end_page = $max_page;
            }
            if ($start_page <= 0) {
                $start_page = 1;
            }
            echo $before . '<nav class="page-navigation"><ul class="pagination">' . "";
            if ($start_page >= 2 && $pages_to_show < $max_page) {
                $first_page_text = __('First', 'foundation');
                echo '<li><a href="' . get_pagenum_link() . '" title="' . $first_page_text . '">' . $first_page_text . '</a></li>';
            }
            echo '<li>';
            previous_posts_link(__('Previous', 'foundation'));
            echo '</li>';
            for ($i = $start_page; $i <= $end_page; $i++) {
                if ($i == $paged) {
                    echo '<li class="current"> ' . $i . ' </li>';
                } else {
                    echo '<li><a href="' . get_pagenum_link($i) . '">' . $i . '</a></li>';
                }
            }
            echo '<li>';
            next_posts_link(__('Next', 'foundation'), 0);
            echo '</li>';
            if ($end_page < $max_page) {
                $last_page_text = __('Last', 'foundation');
                echo '<li><a href="' . get_pagenum_link($max_page) . '" title="' . $last_page_text . '">' . $last_page_text . '</a></li>';
            }
            echo '</ul></nav>' . $after . '';
        }
endif;
