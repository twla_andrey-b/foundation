# TWLA Foundation boilerplate theme #

This is main dev repository of Foundation theme.
It synced with theme installed on [http://ready-for-feedback.com/development/fn-starter-kit](http://ready-for-feedback.com/development/fn-starter-kit/). Use Duplicator to deploy Starter kit when create new project.

To install the theme separately follow next steps:


```
#!git

ssh {{YOUR_LOGIN}}@192.168.78.8
pw: ******************
Shell (2)
cd {{CUSTOMER_NAME}}/{{PROJECT_NAME}}/wp-content/themes/
git clone https://bitbucket.org/twladevelopment/foundation.git theme-name
```